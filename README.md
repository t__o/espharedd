# ESP32 stuff

You will find here different ESP32 project

## Equalizer

Port of https://github.com/G6EJD/ESP32-8266-Audio-Spectrum-Display for Wemos D1
 64x48 oled shield
 
## Rotobo32

Rotobo is a POV installation with 2 parts
- a reflector spinning using a dc motor
- a ws2812 stroboscope pointing to the reflector

Rotation speed and stroboscope variations create some a POV animation on the reflector.
Changing the strobo parameters change the animation

Currently the parameters are controlled using Bluetooth and Android app Bluetooth electronic
 
Parameters are 
- motor speed
- strobo speed
- strobo number of colors
- strobo brigthness

You need to install Bluetooth electronic and u need to install the app and load panel.kwl
Then you need to connect the app to the ESP, then run the panel.



