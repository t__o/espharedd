/*
 * NeoTio.cpp
 *
 *  Created on: Apr 16, 2019
 *      Author: theo
 */

#include "NeoTio.h"
//
//NeoTio::NeoTio() {
//	// TODO Auto-generated constructor stub
//
//}
//
//NeoTio::~NeoTio() {
//	// TODO Auto-generated destructor stub
//}



uint32_t NeoTio::Wheel(byte WheelPos) {
	bool debug = false ;

	if (debug ) Serial.print(" > > color " + String(WheelPos) + " " );
	WheelPos = 255 - WheelPos;

	if(WheelPos < 85) {
		byte vup= 255 - WheelPos * 3;
		byte vdo= WheelPos * 3;

		if (debug ) Serial.println(
				String(vup) + "," +
				String (0) + "," +
				String(vdo ));

		return Adafruit_NeoPixel::Color(vup, 0, vdo);
	}
	if(WheelPos < 170) {
		WheelPos -= 85;
		byte vup= 255 - WheelPos * 3;
		byte vdo= WheelPos * 3;
		if (debug ) Serial.println(
				String(0) + ", " +
				String (vdo) + ", " +
				String(vup ));
		return Adafruit_NeoPixel::Color(0, vdo, vup);
	}
	WheelPos -= 170;
	byte vup= 255 - WheelPos * 3;
	byte vdo= WheelPos * 3;

	if (debug ) Serial.println(
			String(vdo) + ", " +
			String (vup) + ", " +
			String(0 ));

	return Adafruit_NeoPixel::Color(vdo, vup, 0);

}

/*
 *  Colors
> > color 0 255, 0, 0	    red
> > color 16 207, 48, 0	oran
> > color 32 159, 96, 0	yellow
> > color 48 111, 144, 0	green yellow
> > color 64 63, 192, 0	green pale
> > color 80 15, 240, 0	green
> > color 96 0, 222, 33	gereen turquoise
> > color 112 0, 174, 81	turquoise
> > color 128 0, 126, 129	blue light
> > color 144 0, 78, 177	blue
> > color 160 0, 30, 225	blue dark
> > color 176 18,0,237		blue dark purple
> > color 192 66,0,189	purple		viol
> > color 208 114,0,141	pink
> > color 224 162,0,93		pink strong
> > color 240 210,0,45	    pink red

 */

void NeoTio::colorsCycle(int wait) {
	uint16_t i, j;
	for(j=0; j<256; j = j+16) {
		uint32_t  col = Wheel((j-1)& 255);
		for(i=0; i<Adafruit_NeoPixel::numPixels(); i++) {
			Adafruit_NeoPixel::setPixelColor(i,col);
		}
		Adafruit_NeoPixel::show();
		delay(wait);
	}

}

void NeoTio::setWheelColor(byte color) {

	uint16_t i;
	//Serial.println("  setWheelColor " + String( (color-1) & 255  ));
	uint32_t  col = Wheel((color-1)& 255);
	//Serial.println("  colcolcol " + String( col));

	for(i=0; i<Adafruit_NeoPixel::numPixels(); i++) {
		Adafruit_NeoPixel::setPixelColor(i,col  );
	}
	Adafruit_NeoPixel::show();


}

void NeoTio::setOff() {

	uint16_t i;
	for(i=0; i<Adafruit_NeoPixel::numPixels(); i++) {
		Adafruit_NeoPixel::setPixelColor(i,Adafruit_NeoPixel::Color(0, 0, 0, 0));
	}
	Adafruit_NeoPixel::show();
}


int NeoTio::randomSrDelay(){
	// Micro secondes delay
	int delay = 0;
	byte deltype = random(5);
	switch (deltype){
	case 0:
		delay = random(0,2000);
		break;
	case 1:
		delay = random(2000,5000);
		break;
	case 2:
		delay = random(5000,10000);
		break;
	case 3:
		delay = random(10000,17000);
		break;
	case 4:
		delay = random(17000,30000);
		break;

	}
	return delay;
}



void NeoTio::strob(byte colors[], byte colorsCount, int wait) {
	uint16_t i;
	for(i=0; i<colorsCount; i++) {
		if (false )Serial.println(" col i " + String( i));
		setWheelColor(colors[i]);
		delayMicroseconds(wait);
		setOff ();
		delayMicroseconds(wait);
	}
}


void NeoTio::strobTimeMilli(unsigned long now,bool isMicro, byte colors[], byte colorsCount, int wait) {
	unsigned long timer = isMicro ? stroboTimerMicro : stroboTimer ;
	if ( now - timer > wait){	// using a delay
		//Serial.println(" " );
		//Serial.println("  n " + String( now));

		if (isMicro){
			stroboTimerMicro = now;
		}else{
			stroboTimer = now;
		}
		stroboOn = ! stroboOn;				// invert
		if (stroboOn) {
			setWheelColor(colors[colorCounter]);  			// set current color
			colorCounter++;									// advance counter and reset
			colorCounter = colorCounter > colorsCount-1 ? 0 : colorCounter;
			bool debug = false ;
			if (debug){
				Serial.println("  set color " + String( colorCounter) +
						"  col " + String( colors[colorCounter])
				);
			}

		} else setOff ();
	}
}



void NeoTio::strobCol (unsigned long now,int speed,int colorsCount){

	if ( now - randomTimer > randomDelay){
		randomTimer = now;
		randomColors(colorsCount);
	}
	NeoTio::strobTimeMilli(now,stroboTimer,colors, colorsCount, speed);
}

void NeoTio::randomColors(byte count){
	for(byte i=0; i<count; i++) {
		this->colors[i]= random(0,255);
	}
}


void NeoTio::strobRandom (unsigned long nowMicro){
	/*
	 * what to randomise
	 * - flash duration   micro milli long
	 * - period duration
	 * - color count
	 * - colors
	 *
	 * */
	// flash duration


	// lower the delay every changeDelay
	if ( nowMicro - randomTimer > randomDelay * 1000){
		randomTimer = nowMicro;
		byte randomChange = random(3);

		//if (randomChange == 0) randomDelay = 2000; //random(5000,10000);// 500;
		if (randomChange == 0)  stroboDelay = randomSrDelay(); //random(0,500); speedLoop() ; //
		if (randomChange == 1)  colorsCount = random(3,9);
		if (randomChange == 2)  randomColors(colorsCount);

		bool debug = true ;
		if (debug){
			Serial.print(" change " + String(randomChange ));

			Serial.print(" randomDelay " + String(randomDelay ));
			Serial.print(" strobDelay " + String(stroboDelay ));
			Serial.print(" colorCount " + String( colorsCount));

			for(byte i=0; i<colorsCount; i++) {
				if (debug)Serial.print(" c" + String( i) + "  " + String( colors[i]) );
			}

		}

		if (debug)Serial.println("");

	}

	NeoTio::strobTimeMilli(nowMicro,true, colors, colorsCount, stroboDelay);
}


byte memStrobDelay = 0; // 4
byte memColorCount = 4; // 1
byte memColors = 5;		// 1


void NeoTio::setupMem(byte length){
	// put your setup code here, to run once:
	//Serial.begin(115200);
	Serial.println("\nTesting EEPROM Library\n");
	if (!EEPROM.begin(length)) {
		Serial.println("Failed to initialise EEPROM");
		Serial.println("Restarting...");
		delay(1000);
		ESP.restart();
	}
}

void NeoTio::readMem(byte id){
	// delay is INT other are byte
	NeoTio::stroboDelay = EEPROM.readInt(id+memStrobDelay);
	NeoTio::colorsCount = EEPROM.read(id+memColorCount);
	for(byte i=0; i<NeoTio::colorsCount; i++) {
		NeoTio::colors[i] = EEPROM.read(id+memColors+i);
		Serial.println(" read col " + String(NeoTio::colors[i] ));

	}

	Serial.println(" readd");

	Serial.print(" read strobDelay " + String(NeoTio::stroboDelay ));
	Serial.println(" read colorCount " + String(NeoTio::colorsCount));


}
void NeoTio::writeMem(byte id){
	// delay is INT other are byte
	EEPROM.writeInt(id+memStrobDelay,NeoTio::stroboDelay);
	EEPROM.write(id+memColorCount,NeoTio::colorsCount );
	for(byte i=0; i<NeoTio::colorsCount; i++) {
		EEPROM.write(id+memColors+i,NeoTio::colors[i]);
		Serial.println(" write col " + String(i) +" " +String(NeoTio::colors[i] ));
	}

	Serial.println(" write");

	Serial.print(" write strobDelay " + String(NeoTio::stroboDelay ));
	Serial.println(" write colorCount " + String(NeoTio::colorsCount));
};

