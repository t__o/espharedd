/*
 * NeoTio.h
 *
 *  Created on: Apr 16, 2019
 *      Author: theo
 */

#ifndef ROTOBO32_NEOTIO_H_
#define ROTOBO32_NEOTIO_H_

#include <WS2812FX.h>

#include <Arduino.h>
#include <EEPROM.h>

const byte maxColorsCount = 10;

class NeoTio: public WS2812FX {

public:
	NeoTio(uint16_t n, uint8_t p, neoPixelType t) : WS2812FX(n,p,t){};
	//virtual ~NeoTio();
	void strobRandom (unsigned long now);
	void strob (byte colors[],byte colorsCount, int wait);
	void strobCol (unsigned long now,int speed,int colorsCount);
	void strobTimeMilli(unsigned long now, bool isMicro ,byte colors[], byte colorsCount, int wait) ;
	unsigned long stroboTimer;
	unsigned long stroboTimerMicro;

	void setupMem(byte length);
	void readMem(byte id);
	void writeMem(byte id);

protected:
	uint32_t Wheel(byte WheelPos);
	void colorsCycle( int wait );
	void setWheelColor (byte color);
	void setOff ();
	int randomSrDelay();
	void randomColors(byte count);



protected:
	byte colors[maxColorsCount];
	byte colorsCount = maxColorsCount;
	byte colorCounter = 0;
	// strobo
	int stroboDelay = 200;
	bool stroboOn = false;

	// random change
	unsigned long randomTimer;
	int randomDelay = 2000;


};

#endif /* ROTOBO32_NEOTIO_H_ */
