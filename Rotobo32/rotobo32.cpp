#include <Arduino.h>

// ---------------- DC Motor
#define MOTORPIN 16
// Setting PWM properties
const int freq = 30000;
const int pwmChannel = 0;
const int resolution = 8;
int dutyCycle = 100;
int brightness = 200;

unsigned long now;
unsigned long timerSerial=0;
int delaySerial = 1000;
bool switchSeeed = false;



// ---------------- Led Strip
#include "NeoTio.h"
#define LEDPIN 17
#define LEDCOUNT 60
NeoTio nio = NeoTio(LEDCOUNT, LEDPIN, NEO_GRB + NEO_KHZ800);


// ---------------- BlueTooth Control

#include "BluetoothSerial.h"
BluetoothSerial SerialBT;


char data_in; // data received from serial link
int slider_value; // Received Slider Values

//  Auto Generated Code for Arduino IDE

// Use panel.kwl

//  Created using keuwlsofts Bluetooth Electronics App
//  www.keuwl.com

//  Steps:
//  1) Upload code and power your Arduino project
//  2) Run Bluetooth Electronics App and connect to your device
//  3) Press the reset button on your Arduino

//  This auto generated code template is only a suggestion to get you started.
//  It is incomplete and will need modification specific to your project.

bool strobo ;

// ---------------- Setup

void setup(){
	Serial.begin(115200);

	// Motor
	ledcSetup(pwmChannel, freq, resolution);
	ledcAttachPin(MOTORPIN, pwmChannel);

	// Strip
	nio.begin();
	nio.setBrightness(brightness);
	nio.show();
	// memory
	nio.setupMem(128); // 8 * (1 + 4 + 1 + 10 )   8 16

	// setup Bluetoth
	SerialBT.begin("Rotobo");
}

unsigned long nowMicro;
unsigned long randomTimer;
int randomDelay = 2000;
int speed,speedMicro,colorsCount,brigth;
bool speedIsMicro = false;
bool randomOn = true;
int motorLimit = 205 ; //  hardware security
int motorSpeed = 150;

void loop(){
	bool debug = true ;
	now = millis();
	nowMicro = micros();

	// Read the parameters from bluetooth
	if (SerialBT.available()){
		data_in=SerialBT.read();

		// motor
		if(data_in=='M'){
			motorSpeed=SerialBT.parseInt();
			if ( motorSpeed > motorLimit) motorSpeed = motorLimit ;

			if (debug)Serial.println( " motor " + String(motorSpeed));
		}

		if(data_in=='S'){ //  Speed Milli
			speedIsMicro = false;
			nio.stroboTimer = now;
			speed=SerialBT.parseInt();
			if (debug)Serial.println( " speed " + String(speed));
		}

		if(data_in=='T'){ //  Speed Micro
			speedIsMicro = true;
			nio.stroboTimer = nowMicro;
			speedMicro=SerialBT.parseInt();
			//speedMicro = map(speedIsMicro, 0,255,1,35000);
			if (debug)Serial.println( " speedMicro " + String(speedMicro));
			//if (debug)Serial.println( " speedMicro " + String(speedMicro/1000));
		}

		// ColorCount
		if(data_in=='A'){
			colorsCount=SerialBT.parseInt();
			if (debug)Serial.println( " colorsCount " + String(colorsCount));
		}

		if(data_in=='B'){ //  Slider
			brigth=SerialBT.parseInt();
			nio.setBrightness(brigth);
			if (debug)Serial.println( " brigth " + String(brigth));

			//nio.show();
			//<--- Perhaps do something with slider_value here
		}


		if(data_in=='Z'){ //Switch On
			//<--- Insert code for switch on here
			nio.writeMem(1);
		}
		if(data_in=='z'){ // Switch Off
			//<--- Insert code for when switch turned off here
			nio.readMem(1);
		}

		if(data_in=='X'){ //Switch On
			randomOn = true;
			if (debug)Serial.println( " randomOn " + String(randomOn));
		}
		if(data_in=='x'){ // Switch Off
			randomOn = false ;
			if (debug)Serial.println( " randomOn " + String(randomOn));

		}

		if(data_in=='Y'){ //Switch On
			nio.writeMem(2);
		}
		if(data_in=='y'){ // Switch Off
			nio.readMem(2);
		}


	}


	// motor
	ledcWrite(pwmChannel, motorSpeed);

	if (randomOn){
		nio.strobRandom(nowMicro);
	}else{

		// led strobo
		if (speedIsMicro)nio.strobCol(nowMicro, speedMicro,colorsCount);
		else nio.strobCol(nowMicro, speed,colorsCount);

	}
}

