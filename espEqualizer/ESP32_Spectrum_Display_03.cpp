/* ESP32 Audio Spectrum Analyser on an SSD1306/SH1106 Display, 8-bands 125, 250, 500, 1k, 2k, 4k, 8k, 16k
 * Improved noise performance and speed and resolution.
 *####################################################################################################################################
 This software, the ideas and concepts is Copyright (c) David Bird 2018. All rights to this software are reserved.

 Any redistribution or reproduction of any part or all of the contents in any form is prohibited other than the following:
 1. You may print or download to a local hard disk extracts for your personal and non-commercial use only.
 2. You may copy the content to individual third parties for their personal use, but only if you acknowledge the author David Bird as the source of the material.
 3. You may not, except with my express written permission, distribute or commercially exploit the content.
 4. You may not transmit it or store it in any other website or other form of electronic retrieval system for commercial purposes.

 The above copyright ('as annotated') notice and this permission notice shall be included in all copies or substantial portions of the Software and where the
 software use is visible to an end-user.

 THE SOFTWARE IS PROVIDED "AS IS" FOR PRIVATE USE ONLY, IT IS NOT FOR COMMERCIAL USE IN WHOLE OR PART OR CONCEPT. FOR PERSONAL USE IT IS SUPPLIED WITHOUT WARRANTY
 OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHOR OR COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 See more at http://www.dsbird.org.uk

 */

#include <Arduino.h>

#include <Wire.h>
#include "arduinoFFT.h" // Standard Arduino FFT library https://github.com/kosme/arduinoFFT
arduinoFFT FFT = arduinoFFT();
/////////////////////////////////////////////////////////////////////////
// Comment out the display your NOT using e.g. if you have a 1.3" display comment out the SSD1306 library and object
//#include "SH1106.h"     // https://github.com/squix78/esp8266-oled-ssd1306
//SH1106 display(0x3c, 17,16); // 1.3" OLED display object definition (address, SDA, SCL) Connect OLED SDA , SCL pins to ESP SDA, SCL pins

//#include "SSD1306.h"  // https://github.com/squix78/esp8266-oled-ssd1306
//SSD1306 display(0x3c, 16,17);  // 0.96" OLED display object definition (address, SDA, SCL) Connect OLED SDA , SCL pins to ESP SDA, SCL pins

#include "../espEqualizer/Oled.h"
#define BUTTONA 17
#define BUTTONB 16
Oled display(BUTTONA,BUTTONB);

/////////////////////////////////////////////////////////////////////////
#include "../espEqualizer/font.h" // The font.h file must be in the same folder as this sketch
/////////////////////////////////////////////////////////////////////////
#define SAMPLES 1024             // Must be a power of 2
#define SAMPLING_FREQUENCY 40000 // Hz, must be 40000 or less due to ADC conversion time. Determines maximum frequency that can be analysed by the FFT Fmax=sampleF/2.
#define amplitude 150            // Depending on your audio source level, you may need to increase this value
unsigned int sampling_period_us;
unsigned long microseconds;
const int bandsCount = 8 ;
byte peak[bandsCount] = {0,0,0,0,0,0,0,0};
double vReal[SAMPLES];
double vImag[SAMPLES];
unsigned long newTime, oldTime;
int dominant_value;
/////////////////////////////////////////////////////////////////////////

#define SIGNALINPUT 27 // <========== analog entry

int lineColor = 1;


unsigned long dbTimer;
int dbDelay = 100;
bool displayDebug = false ;

void setup() {
	Serial.begin(115200);
	Serial.println("run spectrum");
	display.displayText(20,20,1,"start");
	delay(300);

	display.setupOled();
	sampling_period_us = round(1000000 * (1.0 / SAMPLING_FREQUENCY));
	// sampling_period_us = 1000/40  = 25
}

// 64 x 48
int dmax = 40;   		 // vertical max
int bandLength = 8;		 // band largeur
int bandHeigth = 40 + 10; // max ?

int bandLength2 = 7; // epaisseur ?



void displayBand(int band, int dsize){

	//int dmax = 50;
	dsize /= amplitude;
	if (dsize > dmax) dsize = dmax;
	// dsize max = 50
	//for (int s = 0; s <= dsize; s=s+2){display.display->drawHorizontalLine(1+16*band,64-s, 14);}
	for (int s = 0; s <= dsize; s=s+2){
		display.display->drawFastHLine(1+bandLength*band,bandHeigth-s, bandLength2,lineColor);
	}
	if (dsize > peak[band]) {peak[band] = dsize;}

	if (displayDebug){
		Serial.print(" " + String(band) + " ds " + String( dsize )  + " c " + String( 1+16*band ));
	}
}

void loop() {

	display.display->clearDisplay();
	// we must have only 1 display

	for (int bandi = 0; bandi < bandsCount; bandi++) {
		display.display->setCursor(bandLength*bandi, 0);
		display.display->println(String(bandi+1));
	}

	for (int i = 0; i < SAMPLES; i++) {
		newTime = micros();
		vReal[i] = analogRead(SIGNALINPUT); // Using Arduino ADC nomenclature. A conversion takes about 1uS on an ESP32
		vImag[i] = 0;
		while ((micros() - newTime) < sampling_period_us) { /* do nothing to wait */ }
	}


	FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
	FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
	FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);

	bool dodisplay = false;
	for (int i = 2; i < (SAMPLES/2); i++){ // Don't use sample 0 and only the first SAMPLES/2 are usable.
		// Each array element represents a frequency and its value, is the amplitude. Note the frequencies are not discrete.
		if (vReal[i] > 1000) { // Add a crude noise filter, 10 x amplitude or more
			if (i<=2 )             displayBand(0,(int)vReal[i]); // 125Hz
			if (i >2   && i<=4 )   displayBand(1,(int)vReal[i]); // 250Hz
			if (i >4   && i<=7 )   displayBand(2,(int)vReal[i]); // 500Hz
			if (i >7   && i<=15 )  displayBand(3,(int)vReal[i]); // 1000Hz
			if (i >15  && i<=40 )  displayBand(4,(int)vReal[i]); // 2000Hz
			if (i >40  && i<=70 )  displayBand(5,(int)vReal[i]); // 4000Hz
			if (i >70  && i<=288 ) displayBand(6,(int)vReal[i]); // 8000Hz
			if (i >288           ) displayBand(7,(int)vReal[i]); // 16000Hz


//			dodisplay = true;
//
//			if (displayDebug){
//				Serial.println(String () +  " ----------------- ");
//			}

		}
		for (byte band = 0; band <= 7; band++) display.display->drawFastHLine(1+bandLength*band,bandHeigth-peak[band],bandLength2,lineColor);
	}
	//if (dodisplay)
		display.display->display();
	if (millis()%1 == 0) {for (byte band = 0; band <= 7; band++) {if (peak[band] > 0) peak[band] -= 1;}} // Decay the peak

//	if ( millis() - dbTimer > dbDelay){
//		dbTimer = millis();
//
//		//displayDebug = !displayDebug;
//
////
////		Serial.println(" ===================== " + String( ));
////		Serial.println((int)vReal[0]);
////		Serial.println((int)vReal[100]);
////		Serial.println((int)vReal[200]);
////		Serial.println((int)vReal[300]);
//
//	}

}

