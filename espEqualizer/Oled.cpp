/*
 * Oled.cpp
 *
 *  Created on: Apr 18, 2019
 *      Author: theo
 */
#include "../espEqualizer/Oled.h"

#include <Arduino.h>


void Oled::setupOled(){

	// ESP32 needs pullup
	pinMode(buttonA, INPUT_PULLUP);
	pinMode(buttonB, INPUT_PULLUP);

	display->begin(SSD1306_SWITCHCAPVCC, 0x3C); // initialize with the I2C addr 0x3C (for the 64x48)
	display->display();
	delay(200);
	// Clear the buffer.
	display->clearDisplay();
	display->display();
}

void Oled::displayHemp()
{
	display->drawBitmap(0, 0, hemp, 64, 48, 1);
	display->display();
}

void Oled::displayText(byte x,byte y, byte textSize, String message)
{

	//lineId = (lineId-1)* textSize * 10  ; //1, 11, 21
	display->setTextSize(textSize);
	display->setTextColor(WHITE);
	display->setCursor(x, y);
	display->println(message);
	display->display();
}

void Oled::clear(){
	display->clearDisplay();
	display->display();
}

void Oled::clearOnTimer(unsigned long now){
	if (displayIsActive && now - timerOled > delayOled) {
		Serial.print("clearOnTimerclearOnTimerclearOnTimer");
		timerOled = now;
		displayIsActive = false;
		display->clearDisplay();
		display->display();
	}
}
