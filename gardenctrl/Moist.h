

/// Moist

class Moist {
public:
    Moist(int _pin, int _min, int _max) {
        pin = _pin;
        min = _min;
        max = _max;
    };

    void read();

    int percent;

protected:
    int value;
    int pin;
    int min;
    int max;
};

/* Read, set Min / Max / percent */
void Moist::read() {
    int value = analogRead(pin);
    if (value < min) min = value;
    if (value > max) max = value;

    percent = 100 - map(value, min, max, 0, 100);

    if (false){
        Serial.print(" ");
        Serial.print(value);
        Serial.print(" ");
        Serial.print(percent);
        Serial.print(" ");
        Serial.print(min);
        Serial.print(" ");
        Serial.print(max);
        Serial.println(" ");
    }
};

