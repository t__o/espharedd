/*
 * mqttClient->cpp
 *
 *  Created on: May 9, 2019
 *      Author: theo
 */
#include <Arduino.h>

#include "MtClient.h"

void MtClient::mqttReconnect( unsigned long now) {

	if (  now - mqttRecoTimer > mqttRecoDelay){
		MtClient::mqttRecoTimer = now;

        DEBUGPRINTLN0("Attempting MQTT connection...");
        DEBUGPRINTLN1(mqtt_clientid);
        DEBUGPRINTLN1(mqtt_user);
		DEBUGPRINTLN1(mqtt_password);
		DEBUGPRINTLN1(mqtt_topic_state);

		// Attempt to connect
		if (mqttClient->connect(mqtt_clientid, mqtt_user, mqtt_password, mqtt_topic_state, 1, true, "disconnected", false)) {
			DEBUGPRINTLN0("MQ connected");
			// Once connected, publish an announcement...
			mqttClient->publish(mqtt_topic_state, "connected", true);
		} else {
			DEBUGPRINT0("failed, rc=");
			DEBUGPRINT0(mqttClient->state());
			DEBUGPRINTLN0(" try again in 5 seconds");
		}
	}
}

void MtClient::mqttPublish(const char *topic, float payload) {
	DEBUGPRINT1(topic);
	DEBUGPRINT1(": ");
	DEBUGPRINTLN1(payload);
	mqttClient->publish(topic, String(payload).c_str(), true);
}

void MtClient::mqttSetup(unsigned long now)
{
	DEBUGPRINTLN1( "MQtt connect" + String( mqtt_server));
    DEBUGPRINTLN1( "now" + String( now));
    DEBUGPRINTLN1( "mqttRecoTimer" + String( mqttRecoTimer));
    DEBUGPRINTLN1( "mqttRecoDelay" + String( mqttRecoDelay));

	mqttClient->setServer(mqtt_server, mqtt_port);
	//mqttClient->setCallback(mqttCallback);

	mqttReconnect(now);
}

void MtClient::mqttLoop(unsigned long now){
	if (!mqttClient->connected()) {
		mqttReconnect(now );
	}
	mqttClient->loop();
}


