//
// Created by theo on 06/06/19.
//

#include <Arduino.h>

/// Pins

#define MoistPin1 39            //  1100 to 3200
#define MoistPin2 35            // v2  550  2800
#define DHT221 33


/// Debug

#define DEBUGLEVEL 3

#include "DebugUtils.h"

// **** App Config

#define PUBLISHMQTT true                // display Serial on loop
#define DEEPSLEEP_ENABLE false
#define DEEPSLEEP_DURATION 2 * 60       // 2 minutes
RTC_DATA_ATTR int bootCount = 0;
unsigned long now;

/// MQTT

#include "../../secret/WifiCredential.cpp"
#include "MtClient.h"
#include "WiFi.h"

const char *MQTT_CLIENT_ID = "esp32Garden";
const int MQTT_PORT = 1883;
const char *MQTT_SERVER = "192.168.43.150";     // fUZ ip
//const char *MQTT_SERVER = "192.168.1.16";     // Home ip
const char *MQTT_USER = "mqttuser"; // NULL for no authentication
const char *MQTT_PASSWORD = "mqttpassword"; // NULL for no authentication

const char *MQTT_TOPIC_STATE = "home/garden/status";


WiFiClient espClient;

MtClient Mq(
        espClient,
        MQTT_SERVER,
        MQTT_PORT,
        MQTT_USER,
        MQTT_PASSWORD,
        MQTT_CLIENT_ID,
        MQTT_TOPIC_STATE
);


const char *MQTT_TOPIC_HUMIDITY = "home/garden/humidity";
const char *MQTT_TOPIC_TEMPERATURE = "home/garden/temperature";
const char *MQTT_TOPIC_MOIST1 = "home/garden/moist1";
const char *MQTT_TOPIC_MOIST2 = "home/garden/moist2";


void setupWifi() {

    // Connect to WiFi network
    WiFi.begin(ssid, password);
    DEBUGPRINTLN1("Connecting ");

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        DEBUGPRINT1(".");
    }
    DEBUGPRINTLN1("");
    DEBUGPRINT1("Connected to ");
    DEBUGPRINTLN1(ssid);
    DEBUGPRINT0("IP address: ");
    DEBUGPRINTLN0(WiFi.localIP());


}

/// Moist sensors

#include "Moist.h"

Moist moist1(MoistPin1, 1000, 3200);
Moist moist2(MoistPin2, 550, 3200);


/// dht22

#include <Adafruit_Sensor.h>
#include <DHT.h>

#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHT221, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

float dhtTemp;  //Stores humidity value
float dhtHum; //Stores temperature value

void setupDHT() {
    dht.begin();
}

void readDHT() {
    dhtHum = dht.readHumidity();
    dhtTemp = dht.readTemperature();
}


/// global code


void sensorGet() {

    moist1.read();
    moist2.read();
    readDHT();

    /// display
    DEBUGPRINT2("V1 = ");
    DEBUGPRINT2(moist1.percent);
    DEBUGPRINT2(" V2 = ");
    DEBUGPRINT2(moist2.percent);

    DEBUGPRINT2(" - Temp = ");
    DEBUGPRINT2(dhtTemp);
    DEBUGPRINT2(" Humi = ");
    DEBUGPRINT2(dhtHum);
    DEBUGPRINTLN2();

    if (PUBLISHMQTT) {
        DEBUGPRINTLN0("send data to mqtt");

        Mq.mqttPublish(MQTT_TOPIC_HUMIDITY, dhtHum);
        Mq.mqttPublish(MQTT_TOPIC_TEMPERATURE, dhtTemp);
        //Mq.mqttPublish(MQTT_TOPIC_PRESSURE, PressureBME);
        //Mq.mqttPublish(MQTT_TOPIC_ALTITUDE, AltitudeBME);
        Mq.mqttPublish(MQTT_TOPIC_MOIST1, moist1.percent);
        Mq.mqttPublish(MQTT_TOPIC_MOIST2, moist2.percent);
        DEBUGPRINTLN0("send data to mqtt done");

    }
}


void setup() {
    delay(1000);
    Serial.begin(115200);
    setupDHT();
    setupWifi();
    delay(1000);
    Mq.mqttSetup(millis());

    if (DEEPSLEEP_ENABLE) {

        ++bootCount;
        Serial.println("Boot number: " + String(bootCount));

        sensorGet();

        now = millis();
        Mq.mqttLoop(now);
        delay(1000);         // !! Important or MQTT Fails

        DEBUGPRINTLN1("go sleep");
        // !! Idk is delay is related to CPU or to wifi signal strength ...
        DEBUGPRINTLN0("sleeping after " + String(millis()));
        esp_sleep_enable_timer_wakeup(DEEPSLEEP_DURATION * 1000000);
        Serial.flush();
        esp_deep_sleep_start();
    }
}


unsigned long ltimer;
int ldelay = 1000;

void loop() {

    if (!DEEPSLEEP_ENABLE) {
        now = millis();

        if (now - ltimer > ldelay) {

            ltimer = now;
            sensorGet();

            Serial.println("M oo  o");
        }

        Mq.mqttLoop(now);
    }
    //delay(1500);
}










